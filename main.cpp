#include <SFML/Window/Mouse.hpp>
#include <SFML/System/Vector2.hpp>
#include "include/ant.hpp"
#include "core/simulation.hpp"
#include "core/templates.hpp"

int main(){
	{
		Simulation environment(100000,1,1000,800); //SFML Resizes height in certain cases, explore more
		double start_velocity = .005;
		sf::CircleShape AntShape(5,3);
		AntShape.setFillColor(sf::Color::Red);
		for (int i = 1; i <= 10; i++) {
			start_velocity = -1*start_velocity;
			Ant *a = new Ant();
			a->MoveController.SetPosition(COORDINATE(random<double>(1,999),random<double>(1,799)));
			//a->MoveController.SetPosition(COORDINATE(500,500));
			a->MoveController.SetVelocity(start_velocity);
			environment.AddAgent(a,&AntShape);
		}
		environment.Start();
	}
return 0;
}
