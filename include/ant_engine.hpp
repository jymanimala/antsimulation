#pragma once
#include "core/engine.hpp"

class AntEngine : public Engine {
    public:
        COORDINATE mVelocityLimit = COORDINATE(10,10);
        COORDINATE mAccelerationLimit;
    	AntEngine();
        ~AntEngine();
        void RandomVelocity();
        void RandomHeading();
        void RandomHeading(double lower_limit, double upper_limit);
};
