#pragma once
#include "core/agent.hpp"
#include "ant_engine.hpp"

class Ant : public Agent {
    public:
    	Ant();
    	~Ant();
        AntEngine MoveController;
        void UPDATE();
        void TIME();
};
