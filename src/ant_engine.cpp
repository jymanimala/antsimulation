#include "ant_engine.hpp"
#include "core/templates.hpp"

AntEngine::AntEngine() {

}

AntEngine::~AntEngine() {

}

void AntEngine :: RandomVelocity() {
    mVelocity.x = random<double>(-mVelocityLimit.x,mVelocityLimit.x);
    mVelocity.y = random<double>(-mVelocityLimit.y,mVelocityLimit.y);
}

void AntEngine :: RandomHeading() {
    TurnAngle(random<double>(0,360));
}
void AntEngine :: RandomHeading(double lower_limit, double upper_limit) {
    TurnAngle(random<double>(lower_limit,upper_limit));
}
