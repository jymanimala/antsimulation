#include "ant.hpp"

Ant::Ant() : Agent() {
	functionCallMap["TIME"] = [this]() {TIME();};
	MoveController.mAgentPtr = this;
    mType = "Ant";
	mName = "Ant";
}
Ant::~Ant() {
}


void Ant::UPDATE() {
	//MoveController.RandomVelocity();
	MoveController.RandomHeading(-1,1);
	MoveController.Update(mTime-mLastTime);
	MoveController.LogPosition();
}

void Ant::TIME() {
	mLastTime = mTime;
	mTime = mMessageBuffer.sDouble;
    Ant::UPDATE();
}
